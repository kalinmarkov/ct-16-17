#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Local.h"
using namespace llvm;
namespace {
   struct SimpleDCE : public FunctionPass {
       static char ID;
       SimpleDCE() : FunctionPass(ID) {}
       virtual bool runOnFunction(Function &F) {
       std::map<std::string, int> opCounter;
       errs() << "BEFORE DCE\n";
        for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
            for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
                    opCounter[i->getOpcodeName()] = 1;
                }
                else {
                    opCounter[i->getOpcodeName()] += 1;
                }
            }
        }
        // Does the printing
        std::map <std::string, int>::iterator i = opCounter.begin();
        std::map <std::string, int>::iterator e = opCounter.end();
        while (i != e) {
            errs() << i->first << ": " << i->second << "\n";
            i++;
        }
        opCounter.clear();

        SmallVector<Instruction*, 64> Worklist;
        errs() << "DCE START\n";
        //Eliminate dead code and produce counts here
        //(You can use is instruction trivially dead)
        for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
            for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                if(isInstructionTriviallyDead(&*i)){
                    Worklist.push_back(&*i);            // add every instruction that is trivially dead originally to Worklist
                }
             }
        }

        // eliminate the originally dead instructions, clear the worlklist, but then keep trying to find new ones,
        // as there might be other dead instructions that are not found on the first pass, keep doing this in a loop
        // until no more dead instructions can be identified
        while(!Worklist.empty()){      // loop is similar to recursive calling
         for(Instruction* i : Worklist){
             i->eraseFromParent();
         }

         Worklist.clear();

         for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
             for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                 if(isInstructionTriviallyDead(&*i)){
                     Worklist.push_back(&*i);
                 }
              }
         }
       }
        // once all dead instruction have been removed, just count the number of each type of remaining instruction, just as before
         for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
             for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
                 if(opCounter.find(i->getOpcodeName()) == opCounter.end()){
                     opCounter[i->getOpcodeName()] = 1;
                 }
                 else {
                    opCounter[i->getOpcodeName()] += 1;
                 }
              }
          }

         // print again
         std::map <std::string, int>::iterator i1 = opCounter.begin();
         std::map <std::string, int>::iterator e1 = opCounter.end();
         while (i1 != e1) {
             errs() << i1->first << ": " << i1->second << "\n";
             i1++;
         }
         opCounter.clear();
       
        errs() << "DCE END\n";
        return false;
   }
  };
}
char SimpleDCE::ID = 0;
static RegisterPass<SimpleDCE> X("simpledce", "Simple dead code elimination");
