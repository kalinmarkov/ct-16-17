package ast;

import java.util.LinkedList;
import java.util.List;

/**
 * @author cdubach
 */
public class StructType implements Type {

    public final String str;
    public final List<VarDecl> vardecls;

    public StructType(String str, List<VarDecl> vardecls){
        this.str = str;
        this.vardecls = vardecls;
    }

    public StructType(String str){
        this.str = str;
        this.vardecls = new LinkedList<>();
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitStructType(this);
    }

}
