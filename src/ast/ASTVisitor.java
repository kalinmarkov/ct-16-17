package ast;

public interface ASTVisitor<T> {
    public T visitProgram(Program p);
    public T visitBaseType(BaseType bt);
    public T visitPointerType(PointerType pt);
    public T visitStructType(StructType st);
    public T visitArrayType(ArrayType a);
    public T visitVarDecl(VarDecl vd);
    public T visitFunDecl(FunDecl p);
    public T visitIntLiteral(IntLiteral i);
    public T visitStrLiteral(StrLiteral i);
    public T visitChrLiteral(ChrLiteral c);
    public T visitVarExpr(VarExpr v);
    public T visitFunCallExpr(FunCallExpr f);
    public T visitBinOp(BinOp bo);
    public T visitOp(Op o);
    public T visitArrayAccessExpr(ArrayAccessExpr aae);
    public T visitFieldAccessExpr(FieldAccessExpr fae);
    public T visitValueAtExpr(ValueAtExpr valueAtExpr);
    public T visitSizeOfExpr(SizeOfExpr sizeOfExpr);
    public T visitTypecastExpr(TypecastExpr t);
    public T visitExprStmt(ExprStmt expr);
    public T visitWhile(While w);
    public T visitIf(If i);
    public T visitAssign(Assign a);
    public T visitReturn(Return r);
    public T visitBlock(Block b);

}
