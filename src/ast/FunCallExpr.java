package ast;

import java.util.List;

/**
 * Created by s1339217 on 16/10/16.
 */
public class FunCallExpr extends Expr {
    public final String str;
    public final List<Expr> expr;
    public FunDecl fd; // to be filled in by the name analyser

    public FunCallExpr(String str, List<Expr> expr){
        this.str = str;
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitFunCallExpr(this);
    }

}
