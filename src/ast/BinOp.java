package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class BinOp extends Expr {

    public final Expr e1;
    public final Op o;
    public final Expr e2;
    public Type type;             // to be set by the type analyzer

    public BinOp(Expr e1, Op o, Expr e2){
        this.e1 = e1;
        this.o = o;
        this.e2 = e2;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitBinOp(this);
    }


}
