package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class Return extends Stmt{

    public Expr exprOpt;

    public Return(){
        this.exprOpt = null;
    }

    public Return(Expr exprOpt){
        this.exprOpt = exprOpt;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitReturn(this);
    }

}
