package ast;

import java.io.PrintWriter;

public class ASTPrinter implements ASTVisitor<Void> {

    private PrintWriter writer;

    public ASTPrinter(PrintWriter writer) {
        this.writer = writer;

    }

    @Override
    public Void visitProgram(Program p) {
        System.out.println("AAAAAAAAAAAAAAAA!!!!!!!!!");
        writer.print("Program(");
        String delimiter = "";
        for (StructType st : p.structTypes) {
            writer.print(delimiter);
            delimiter = ",";
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            writer.print(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            writer.print(delimiter);
            delimiter = ",";
            fd.accept(this);
        }
        writer.print(")");
        writer.flush();
        return null;
    }

    @Override
    public Void visitBaseType(BaseType bt) {
        //writer.print("BaseType(");
        writer.print(bt.toString());
        //writer.print(")");
        return null;
    }

    @Override
    public Void visitPointerType(PointerType pt) {
        writer.print("PointerType(");
        pt.type.accept(this);
        writer.print(")");
        writer.flush();
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        writer.print("StructType(");
        writer.print(st.str);
        String delimiter = "";
        if(st.vardecls.size() > 0){
            writer.print(",");
        }
        for (VarDecl vd : st.vardecls) {
            writer.print(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        writer.print(")");
        writer.flush();
        return null;
    }

    @Override
    public Void visitArrayType(ArrayType a) {
        writer.print("ArrayType(");
        a.type.accept(this);
        writer.print("," + a.numElements);
        writer.print(")");
        return null;
    }


    @Override
    public Void visitVarDecl(VarDecl vd) {
        writer.print("VarDecl(");
        vd.type.accept(this);
        writer.print("," + vd.varName);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitFunDecl(FunDecl fd) {
        writer.print("FunDecl(");
        fd.type.accept(this);
        writer.print("," + fd.name + ",");
        String delimiter = "";
        for (VarDecl vd : fd.params) {
            writer.print(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        writer.print(delimiter);
        fd.block.accept(this);
        writer.print(")");
        writer.flush();
        return null;
    }

    @Override
    public Void visitIntLiteral(IntLiteral i) {
        writer.print("IntLiteral(");
        writer.print(i.i);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitStrLiteral(StrLiteral str) {
        writer.print("StrLiteral(");
        writer.print(str.s);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitChrLiteral(ChrLiteral chr) {
        writer.print("ChrLiteral(");
        System.out.println(chr.c);
        writer.print(chr.c);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        writer.print("VarExpr(");
        writer.print(v.name);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitFunCallExpr(FunCallExpr f) {
        writer.print("FunCallExpr(");
        writer.print(f.str);
        String delimiter = "";
        if(f.expr.size() > 0){
            writer.print(",");
        }
        for (Expr exp : f.expr) {
            writer.print(delimiter);
            delimiter = ",";
            exp.accept(this);
        }
        writer.print(")");
        writer.flush();
        return null;
    }

    @Override
    public Void visitBinOp(BinOp bp) {
        writer.print("BinOp(");
        bp.e1.accept(this);
        writer.print(",");
        writer.print(bp.o);
        writer.print(",");
        bp.e2.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitOp(Op o) {
        writer.print("Op(");
        writer.print(o.toString());
        writer.print(")");
        return null;
    }

    @Override
    public Void visitArrayAccessExpr(ArrayAccessExpr aae) {
        writer.print("ArrayAccessExpr(");
        aae.array.accept(this);
        writer.print(",");
        aae.index.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitFieldAccessExpr(FieldAccessExpr fae) {
        writer.print("FieldAccessExpr(");
        fae.exp.accept(this);
        writer.print(",");
        writer.print(fae.str);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitValueAtExpr(ValueAtExpr vae) {
        writer.print("ValueAtExpr(");
        vae.expr.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitSizeOfExpr(SizeOfExpr soe) {
        writer.print("SizeOfExpr(");
        soe.t.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitTypecastExpr(TypecastExpr te) {
        writer.print("TypecastExpr(");
        te.t.accept(this);
        writer.print(",");
        te.e.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitExprStmt(ExprStmt es) {
        writer.print("ExprStmt(");
        es.expr.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitWhile(While w) {
        writer.print("While(");
        w.expr.accept(this);
        writer.print(",");
        w.stmt.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitIf(If i) {
        writer.print("If(");
        i.expr.accept(this);
        writer.print(",");
        i.stmt.accept(this);
        if (i.stmtOptional != null) {
            writer.print(",");
            i.stmtOptional.accept(this);
        }
        writer.print(")");
        return null;
    }

    @Override
    public Void visitAssign(Assign a) {
        writer.print("Assign(");
        a.expr1.accept(this);
        writer.print(",");
        a.expr2.accept(this);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitReturn(Return r) {
        writer.print("Return(");
        if (r.exprOpt != null) {
            r.exprOpt.accept(this);
        }
        writer.print(")");
        return null;
    }

    @Override
    public Void visitBlock(Block b) {
        writer.print("Block(");
        String delimiter = "";
        for (VarDecl v : b.varDecls) {
            writer.print(delimiter);
            delimiter = ",";
            v.accept(this);
        }
        /*if (b.stmts.size() > 0 && b.varDecls.size() == 0) {
            writer.print(",");
        }*/
        for (Stmt s : b.stmts) {
            writer.print(delimiter);
            delimiter = ",";
            s.accept(this);
        }
        writer.print(")");
        writer.flush();
        return null;
    }
}
