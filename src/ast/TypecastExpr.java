package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class TypecastExpr extends Expr{

    public final Type t;
    public final Expr e;

    public TypecastExpr(Type t,Expr e){
        this.t = t;
        this.e = e;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitTypecastExpr(this);
    }

}
