package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class SizeOfExpr extends Expr{
    public final Type t;

    public SizeOfExpr(Type t){
        this.t = t;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitSizeOfExpr(this);
    }

}
