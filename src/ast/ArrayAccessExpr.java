package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class ArrayAccessExpr extends Expr {
    public final Expr array;
    public final Expr index;

    public ArrayAccessExpr(Expr array,Expr index){
        this.array = array;
        this.index = index;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitArrayAccessExpr(this);
    }
}
