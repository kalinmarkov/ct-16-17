package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class ValueAtExpr extends Expr {
    public final Expr expr;

    public ValueAtExpr(Expr expr){
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitValueAtExpr(this);
    }
}
