package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class If extends Stmt {

    public final Expr expr;
    public final Stmt stmt;
    public final Stmt stmtOptional;

    public If(Expr expr,Stmt stmt,Stmt stmtOptional){
        this.expr = expr;
        this.stmt = stmt;
        this.stmtOptional = stmtOptional;
    }

    public If(Expr expr,Stmt stmt){
        this.expr = expr;
        this.stmt = stmt;
        this.stmtOptional = null;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitIf(this);
    }

}
