package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class ExprStmt extends Stmt{

    public final Expr expr;

    public ExprStmt(Expr expr){
        this.expr = expr;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitExprStmt(this);
    }

}

