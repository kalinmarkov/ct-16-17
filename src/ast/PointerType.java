package ast;

/**
 * Created by s1339217 on 16/10/16.
 */
public class PointerType implements Type{
    public final Type type;

    public PointerType(Type type){
        this.type = type;
    }

    public <T> T accept(ASTVisitor<T> v) {
        return v.visitPointerType(this);
    }

}
