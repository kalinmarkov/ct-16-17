package sem;

import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Symbol> symbolTable = new HashMap<String, Symbol>();

	
	public Scope(Scope outer) { 
		this.outer = outer; 
	}
	
	public Scope() {

	}

	public Map<String, Symbol> getSymbolTable(){
		return symbolTable;
	}

	public Symbol lookup(String name) {
		Symbol s = lookupCurrent(name);
		if(s != null){
			return s;
		}
		if(s == null && outer != null){
			return outer.lookup(name);
		}
		return null;
	}
	
	public Symbol lookupCurrent(String name) {
		if(symbolTable.get(name) != null){
			return symbolTable.get(name);
		}
		else
			return null;
	}
	
	public void put(Symbol sym) {
		symbolTable.put(sym.name, sym);
	}
}
