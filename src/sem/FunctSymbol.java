package sem;

import ast.FunDecl;
import ast.VarDecl;

/**
 * Created by s1339217 on 20/10/16.
 */
public class FunctSymbol extends Symbol {
    FunDecl fd;

    public FunctSymbol(FunDecl fd){
        super(fd.name);
        this.fd = fd;
    }

    public boolean isFunct(){
        return true;
    }
}
