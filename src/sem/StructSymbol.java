package sem;

import ast.StructType;
import ast.VarDecl;

import java.sql.Struct;

/**
 * Created by s1339217 on 20/10/16.
 */
public class StructSymbol extends Symbol {

    StructType st;

    public StructSymbol(StructType st){
        super(st.str);
        this.st = st;
    }

    public boolean isStruct(){
        return true;
    }
}
