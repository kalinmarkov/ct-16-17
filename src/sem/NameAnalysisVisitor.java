package sem;

import ast.*;
import lexer.Token;

import java.util.*;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {

	Scope scope;

    private Map<String, Symbol> functParameters = new HashMap<String,Symbol>();

	public NameAnalysisVisitor(Scope scope){
		this.scope = scope;
	}

    public NameAnalysisVisitor(){
        this.scope = null;
    }

	@Override
	public Void visitBaseType(BaseType bt) {
		return null;
	}

	@Override
	public Void visitStructType(StructType st) {
		System.out.println("Goes in structype method");
		for(VarDecl vd : st.vardecls){
            vd.accept(this);
        }
		return null;
	}

	@Override
	public Void visitBlock(Block b) {
        System.out.println("Goes in the BLOCK method");

		Scope oldScope = scope;    // the oldScope is the outer scope
		scope = new Scope(scope);         // scope now refers to the inner scope
        System.out.println(functParameters);
        for(Map.Entry<String, Symbol> entry: functParameters.entrySet()) {
            scope.put(entry.getValue());
        }

        for (VarDecl v : b.varDecls) {
            v.accept(this);
        }

		for(Stmt s : b.stmts){
			if(s instanceof Block){
				functParameters.clear();
			}
			s.accept(this); // visit statement s
		}

		scope = oldScope;          // reassign scope to point to the outer one
		return null;
	}

	@Override
	public Void visitFunDecl(FunDecl p) {
		System.out.println("Goes in fundecl method");
		System.out.println(scope);

		Symbol s = scope.lookupCurrent(p.name);

		if (s != null) {
            error(p.name + " already declared"); // already declared and shouldn't be declared again - error!
        } else {
            scope.put(new FunctSymbol(p));  // fine - put it in dictionary
        }
        for (VarDecl v : p.params) {
            functParameters.put(v.varName, new VarSymbol(v));
        }
        p.block.accept(this);

        functParameters.clear();

        return null;
    }


	@Override
	public Void visitProgram(Program p) {
		System.out.println("Goes in this program---------------------------------------");
		scope = new Scope();

        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }

        for (FunDecl fd : p.funDecls) {
			System.out.println("Goes in here");
			System.out.println(fd.name);
            fd.accept(this);
        }

        return null;
	}

	@Override
	public Void visitVarDecl(VarDecl vd) {
		Symbol s = scope.lookupCurrent(vd.varName);
		if(s != null){
			error(vd.varName + " already declared"); // already declared and shouldn't be declared again - error!
		}
        else{
			scope.put(new VarSymbol(vd));  // fine - put it in dictionary
		}
		return null;
	}

	// this is the use of a variable
	@Override
	public Void visitVarExpr(VarExpr v) {
        System.out.println("Should go here");
        Symbol s = scope.lookup(v.name);    // we are trying to use the variable - so it must have been declared
        if(s == null){
            error(v.name + " has not been declared");
        }
        else if(!s.isVar()){
            error(s.name + " is not a variable");
        }
        else{
            System.out.println("Must go here");
            System.out.println(v.name);
            v.vd = ((VarSymbol)s).vd;      // this is where we should enter
            System.out.println(v.vd.type);
        }
		return null;
	}

	@Override
	public Void visitPointerType(PointerType p) {
        p.type.accept(this);
		return null;
	}

	@Override
	public Void visitArrayType(ArrayType a) {
		a.type.accept(this);
        return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral i) {
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral i) {
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral c) {
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr f) {
        // Add the print_s function to the scope
		// aArrayType initialized to a random value of 5, should not really matter
        VarDecl v = new VarDecl(new PointerType(BaseType.CHAR),"s");
        List<VarDecl> list = new ArrayList<VarDecl>();
        list.add(v);
        FunDecl funDecl = new FunDecl(BaseType.VOID,"print_s",list,null);
        scope.put(new FunctSymbol(funDecl));


        // Add the print_i function to the scope
        VarDecl v1 = new VarDecl(BaseType.INT,"i");
        List<VarDecl> list1 = new ArrayList<VarDecl>();
        list1.add(v1);
        FunDecl funDecl1 = new FunDecl(BaseType.VOID,"print_i",list1,null);
        scope.put(new FunctSymbol(funDecl1));


        // Add the print_c function to the scope
        VarDecl v2 = new VarDecl(BaseType.CHAR,"c");
        List<VarDecl> list2 = new ArrayList<VarDecl>();
        list2.add(v2);
        FunDecl funDecl2 = new FunDecl(BaseType.VOID,"print_c",list2,null);
        scope.put(new FunctSymbol(funDecl2));


        // Add the read_c function to the scope
        FunDecl funDecl3 = new FunDecl(BaseType.CHAR,"read_c",null,null);
        scope.put(new FunctSymbol(funDecl3));


        // Add the read_i function to the scope
        FunDecl funDecl4 = new FunDecl(BaseType.INT,"read_i",null,null);
        scope.put(new FunctSymbol(funDecl4));


        // Add the mcmalloc function to the scope
        VarDecl v3 = new VarDecl(new PointerType(BaseType.INT),"size");
        List<VarDecl> list3 = new ArrayList<VarDecl>();
        list3.add(v3);
        FunDecl funDecl5 = new FunDecl(new PointerType(BaseType.VOID),"mcmalloc",list3,null);
        scope.put(new FunctSymbol(funDecl5));

        System.out.println("Visits FUNCALLEXPR");
		Symbol s = scope.lookup(f.str);

        if(s == null){
            error(f.str + " has not been declared");
        }
        else if(!s.isFunct()){
			System.out.println("Goes here!");
            error(s.name + " is not a function");
        }
        else{
            System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
            f.fd = ((FunctSymbol)s).fd;
            // need to check if function arguments are valid and have been declared in the scope
            for(Expr e : f.expr){
                e.accept(this);
            }
        }
        return null;

	}

	@Override
	public Void visitBinOp(BinOp bo) {
		bo.e1.accept(this);
        bo.e2.accept(this);
		return null;
	}

	@Override
	public Void visitOp(Op o) {
		return null;
	}

	@Override
	public Void visitArrayAccessExpr(ArrayAccessExpr aae) {
		aae.array.accept(this);
        aae.index.accept(this);
        return null;
	}

	@Override
	public Void visitFieldAccessExpr(FieldAccessExpr fae) {
        fae.exp.accept(this);
        return null;
	}

	@Override
	public Void visitValueAtExpr(ValueAtExpr v) {
        v.expr.accept(this);
		return null;
	}

	@Override
	public Void visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
		sizeOfExpr.accept(this);
		return null;
	}

	@Override
	public Void visitTypecastExpr(TypecastExpr t) {
		t.t.accept(this);
        t.e.accept(this);
		return null;
	}

	@Override
	public Void visitExprStmt(ExprStmt expr) {
        expr.expr.accept(this);
		return null;
	}

	@Override
	public Void visitWhile(While w) {
		w.expr.accept(this);
        w.stmt.accept(this);
		return null;
	}

	@Override
	public Void visitIf(If i) {
		i.expr.accept(this);
        i.stmt.accept(this);
        if (i.stmtOptional != null) {
            i.stmtOptional.accept(this);
        }
		return null;
	}

	@Override
	public Void visitAssign(Assign a) {
		a.expr1.accept(this);
        a.expr2.accept(this);
		return null;
	}

	@Override
	public Void visitReturn(Return r) {
        System.out.println("Visited return statement");
        if (r.exprOpt != null) {
            r.exprOpt.accept(this);
        }
		return null;
	}
}
