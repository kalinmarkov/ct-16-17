package sem;

public abstract class Symbol {
	public String name;
	
	public Symbol(){
		this.name = null;
	}

	public Symbol(String name) {
		this.name = name;
	}

	public boolean isVar(){
        return false;
    }

    public boolean isFunct(){
		return false;
    }

	public boolean isStruct(){
		return false;
	}
}
