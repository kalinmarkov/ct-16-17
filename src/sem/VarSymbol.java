package sem;

import ast.VarDecl;

/**
 * Created by s1339217 on 20/10/16.
 */
public class VarSymbol extends Symbol {
    VarDecl vd;

    public VarSymbol(VarDecl vd){
        super(vd.varName);
        this.vd = vd;
    }

    public boolean isVar(){
        return true;
    }

}
