package sem;

import ast.*;
import ast.Type;
import lexer.Token;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {

    List<Type> listReturnTypes = new ArrayList<Type>();  // list to hold the return types of a function


    @Override
	public Type visitBaseType(BaseType bt) {
		return bt;
	}

	@Override
	public Type visitStructType(StructType st) {
		if(st != null){
            return st;
        }
		return null;
	}

	@Override
	public Type visitBlock(Block b) {
        System.out.println("Goes in block");
        for(VarDecl s : b.varDecls){
            System.out.println("ok");
            s.accept(this);
        }

		for(Stmt s : b.stmts){
            System.out.println("yes");
            s.accept(this);
        }

		return null;
	}

	@Override
	public Type visitFunDecl(FunDecl p) {
		System.out.println("Goes in funDecl");
        List<Type> list = investigateBlockReturnType(p.block);
        System.out.println("MAIKAMUDEBA: " + list.toString());
        for(Type t : list){
            System.out.println(t);
            if(p.type != t){
                error("Function return type and signature return type do not match");
            }
        }
        listReturnTypes.clear();
        return p.block.accept(this);
	}

	public List<Type> investigateBlockReturnType(Block b){
        System.out.println("Investigation");


        for(Stmt s : b.stmts) {
            System.out.println("VERMONT " + s);
            System.out.println(s instanceof Return);
            if (s instanceof Return) {// || s instanceof Block || s instanceof If || s instanceof While)
                System.out.println("AHAHAHA");
                listReturnTypes.add(visitReturn((Return) s));
            }
            if(s instanceof Block){
                System.out.println("WENT IN BLOCK");
                listReturnTypes.addAll(investigateBlockReturnType((Block)(s)));
            }
            if(s instanceof If){
                System.out.println("WENT IN IF");
                if(((If) s).stmt instanceof Block){
                    listReturnTypes.addAll(investigateBlockReturnType((Block)(((If) s).stmt)));
                }
                if(((If) s).stmt instanceof Return){
                    listReturnTypes.add(visitReturn((Return)((If) s).stmt));
                }
                if(((If) s).stmtOptional != null && ((If) s).stmtOptional instanceof Block){
                    listReturnTypes.addAll(investigateBlockReturnType((Block)(((If) s).stmtOptional)));
                }
            }
        }
        if(listReturnTypes.size() == 0){
            listReturnTypes.add(BaseType.VOID);
        }
        return listReturnTypes;
    }


	@Override
	public Type visitProgram(Program p) {
        System.out.println("Goes in the type check visitor");
        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
		return null;
	}

	@Override
	public Type visitVarDecl(VarDecl vd) {
        if(vd.type == BaseType.VOID)
            error("Cannot have a void vardecl");

        return null;
	}

	@Override
	public Type visitVarExpr(VarExpr v) {
        System.out.println("VISITS VAR EXPR");

        if(v.vd == null) {
            return null;
        }
        v.type = v.vd.type;

        return v.vd.type;
	}

	@Override
	public Type visitPointerType(PointerType p) {
		System.out.println("VISITS POINTER TYPE");
        return p;
	}

	@Override
	public Type visitArrayType(ArrayType a) {
        System.out.println("VISIT ARRAY TYPE");
        if(a != null){
            return a.type;
        }
        return null;
	}

	@Override
	public Type visitIntLiteral(IntLiteral i) {
		i.type = BaseType.INT;
        return BaseType.INT;
	}

	@Override
	public Type visitStrLiteral(StrLiteral i) {
        String s = i.s;
        i.type = new ArrayType(BaseType.CHAR,s.length());
		return new ArrayType(BaseType.CHAR,s.length());
	}

	@Override
	public Type visitChrLiteral(ChrLiteral c) {
		c.type = BaseType.CHAR;
        return BaseType.CHAR;
	}

	@Override
	public Type visitFunCallExpr(FunCallExpr f) {
        System.out.println("Visits fun call expression");
        if(f.fd == null){
            return null;
        }

        if(f.expr.size() == 0 && f.fd.params == null){
            f.type = f.fd.type;
            return f.fd.type;
        }

        if(f.fd.params != null && f.expr.size() == f.fd.params.size()){    // correct number of arguments
            f.type = f.fd.type;
            int index = 0;
            for(Expr e : f.expr){
                System.out.println("This argument is of type:" + e.accept(this));
                if(e.accept(this) == null){
                    return null;
                }


                if(!(f.fd.params.get(index).type == e.accept(this))) {
                    error("One of the function arguments is of incorect type!!!");
                    return null;
                }

                index = index + 1;
            }
            return f.fd.type;
        }
        else{
            error("Incorrect number of arguments given!");
        }
		return null;
	}

	@Override
	public Type visitBinOp(BinOp bo) {
		//ADD, SUB, MUL, DIV, MOD, GT, LT, GE, LE, NE, EQ, OR, AND
		Type lhsT = bo.e1.accept(this);
		Type rhsT = bo.e2.accept(this);
		if(bo.o == Op.ADD || bo.o == Op.SUB || bo.o == Op.MUL || bo.o == Op.DIV
				|| bo.o == Op.MOD || bo.o == Op.GT || bo.o == Op.LT || bo.o == Op.GE
				|| bo.o == Op.LE || bo.o == Op.OR || bo.o == Op.AND){
            if(lhsT == null || rhsT == null){
                return null;
            }
			if(lhsT.accept(this) == BaseType.INT && rhsT.accept(this) == BaseType.INT){
                bo.type = BaseType.INT;
				return BaseType.INT;
			}
            if(lhsT.accept(this) == BaseType.CHAR && rhsT.accept(this) == BaseType.CHAR){
                bo.type = BaseType.INT;
                return BaseType.INT;
            }
			else{
				error("Cannot " + bo.o.toString() +  " something other than integers or chars");
			}
		}
		if(bo.o == Op.NE || bo.o == Op.EQ){
            if(lhsT.accept(this) == BaseType.INT && rhsT.accept(this) == BaseType.INT){
                bo.type = BaseType.INT;
                return BaseType.INT;
            }
            if(lhsT.accept(this) == BaseType.CHAR && rhsT.accept(this) == BaseType.CHAR){
                bo.type = BaseType.INT;
                return BaseType.INT;
            }
            else{
                error("Cannot " + bo.o.toString() + " something other than integers or chars");
            }
        }

		return null;
	}

	@Override
	public Type visitOp(Op o) {
		return null;
	}

	@Override
	public Type visitArrayAccessExpr(ArrayAccessExpr aae) {
        System.out.println("VISITS ARRAY ACCESS EXPR");
        if((aae.array.accept(this) instanceof ArrayType || aae.array.accept(this) instanceof PointerType) &&
                aae.index.accept(this) == BaseType.INT){
            ArrayType t = (ArrayType)aae.array.accept(this);
            System.out.println("KVOOO" + t.type);
            aae.type = t.type;
            return (t.type);
        }
        else{
            error("Array access is not correct!");
        }
		return null;
	}

	@Override
	public Type visitFieldAccessExpr(FieldAccessExpr fae) {
		if(fae.exp.type instanceof StructType){
            StructType s = (StructType)fae.exp.type;
            for(VarDecl v : s.vardecls){
                if(v.varName.equals(fae.str)){
                    fae.type = v.type;
                }
            }

            return fae.exp.accept(this);
        }
        else{
            error("Field access is not correct!");
        }
		return null;
	}

	@Override
	public Type visitValueAtExpr(ValueAtExpr v) {
        if(v.expr != null) {
            v.type = v.expr.type;
            return v.expr.accept(this);
        }
        return null;
	}

	@Override
	public Type visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
		sizeOfExpr.type = sizeOfExpr.t;
        return BaseType.INT;
	}

	@Override
	public Type visitTypecastExpr(TypecastExpr t) {
		if(t.e instanceof ChrLiteral){
           if(t.t == BaseType.INT){
               t.type = t.t;
               return BaseType.INT;
           }
           else{
               error("Cannot cast a char to something else than an int");
           }
        }
        if(t.e.type instanceof ArrayType){
            Type elemType = t.t.accept(this);
            if(elemType == new PointerType(elemType)){
                t.type = elemType;
                return elemType;
            }
            else{
                error("Cannot cast arraytype to something else than a pointer");
            }
        }
        if(t.e.type instanceof PointerType){
            Type elemType = t.t.accept(this);
            if(elemType != new PointerType(t.e.type)) {
                t.type = elemType;
                return elemType;
            }
            else{
                error("Cannot cast pointer to something else than a pointer");
            }
        }

		return null;
	}

	@Override
	public Type visitExprStmt(ExprStmt expr) {
		if(expr != null) {
            System.out.println("OK");
            return expr.expr.accept(this);
        }
		return null;
	}

	@Override
	public Type visitWhile(While w) {
		if (w.expr.accept(this) == BaseType.INT){
            return w.stmt.accept(this);
        }
        else{
            error("Condition in while look is not of type int");
        }
		return null;
	}

	@Override
	public Type visitIf(If i) {
        if (i.expr.accept(this) == BaseType.INT){
            if(i.stmtOptional == null){
                return i.stmt.accept(this);
            }
            else{
                if(i.stmt.accept(this) == i.stmtOptional.accept(this)){
                    return i.stmt.accept(this);
                }
                if(i.stmt.accept(this) == null){
                    return i.stmtOptional.accept(this);
                }
                if(i.stmtOptional.accept(this) == null){
                    return i.stmt.accept(this);
                }
            }
        }
        else{
            error("Condition in if is not of type int");
        }
        return null;
	}

	@Override
	public Type visitAssign(Assign a) {
        System.out.println("Should go here");
        System.out.println(a.expr1.accept(this));
        System.out.println(a.expr1.toString());
        System.out.println(a.expr2.accept(this));
		if(a.expr1.accept(this) == a.expr2.accept(this) && a.expr1.accept(this) != BaseType.VOID
                && !(a.expr1.accept(this) instanceof ArrayType) && a.expr2.accept(this) != BaseType.VOID
                && !(a.expr2.accept(this) instanceof ArrayType)){
            return a.expr1.accept(this);
        }
        else
            error("Types do not match");

		return null;
	}

	@Override
	public Type visitReturn(Return r) {
        System.out.println("Goes in here");
        if(r.exprOpt != null) {
            return r.exprOpt.accept(this);
        }
		return BaseType.VOID;
	}
}
