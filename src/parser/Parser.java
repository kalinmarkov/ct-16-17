package parser;

import ast.*;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * @author cdubach
 */

public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<Token>();

    private final Tokeniser tokeniser;

    private Expr exprConsumed = null;      // will be used for the ArrayAccessExpr and FieldAccessExpr classes
    private ASTVisitor v;

    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    private int exprErraneousStartCount = 0;

    public Program parse() {
        // get the first token
        nextToken();

        Program p = parseProgram();
        return p;
    }

    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {

        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }
        System.out.println("Parsing error: expected ("+sb+") found ("+token+") at "+token.position);

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */

    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt=1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */

    private void nextToken() {
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */

    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e == token.tokenClass) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }

        error(expected);
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */

    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }

    private Program parseProgram() {
        System.out.println("ENTERS PARSE PROGRAM\n");
        parseIncludeRep();
        List<StructType> sts = parseStructDeclRep();
        System.out.println("Changing point");
        List<VarDecl> vds = parseVarDeclRep();
        List<FunDecl> fds = parseFunDeclRep();
        expect(TokenClass.EOF);
        return new Program(sts, vds, fds);
    }

    private void parseIncludeRep(){
        System.out.println("ENTERS PARSE INCLUDE REP\n");
        if(accept(TokenClass.INCLUDE)) {
            parseInclude();
            parseIncludeRep();
        }
    }

    private List<StructType> parseStructDeclRep() {
        System.out.println("Enters parseStructDeclRep");
        if(lookAhead(2).tokenClass == TokenClass.LBRA) {
            System.out.println("ENTERS PARSE STRUCT DECL REP\n");
            if (accept(TokenClass.STRUCT)) {
                StructType str = parseStructDecl();
                List<StructType> list = parseStructDeclRep();
                list.add(0,str);
                return list;
            }
        }
        return new LinkedList<>();
    }

    private List<VarDecl> parseVarDeclRep(){
        System.out.println("ENTERS PARSE VAR DECL REP\n");
        TokenClass t1 = lookAhead(1).tokenClass;
        TokenClass t2 = lookAhead(2).tokenClass;
        TokenClass t3 = lookAhead(3).tokenClass;
        TokenClass t4 = lookAhead(4).tokenClass;

        boolean cond1 = accept(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID) &&
                (t2 == TokenClass.SC || t2 == TokenClass.LSBR);

        boolean cond2 = accept(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID) &&
                t1 == TokenClass.ASTERIX && (t3 == TokenClass.SC || t3 == TokenClass.LSBR);

        boolean cond3 = accept(TokenClass.STRUCT) && t1 == TokenClass.IDENTIFIER && t2 == TokenClass.IDENTIFIER
                && (t3 == TokenClass.SC || t3 == TokenClass.LSBR);

        boolean cond4 = accept(TokenClass.STRUCT) && t1 == TokenClass.IDENTIFIER && t2 == TokenClass.ASTERIX
                && t3 == TokenClass.IDENTIFIER && (t4 == TokenClass.SC || t4 == TokenClass.LSBR);

        if (cond1 || cond2 || cond3 || cond4) {
            System.out.println("Should not go here");
            VarDecl vd = parseVarDecl();
            List<VarDecl> list = parseVarDeclRep();
            list.add(0,vd);
            return list;
        }
        return new LinkedList<>();
    }

    private List<FunDecl> parseFunDeclRep(){
        System.out.println("ENTERS PARSE FUN DECL REP\n");
        if(accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT) &&
                (lookAhead(3).tokenClass == TokenClass.LPAR || lookAhead(2).tokenClass == TokenClass.LPAR
                        || lookAhead(4).tokenClass == TokenClass.LPAR)){
            FunDecl fd =  parseFunDecls();
            List<FunDecl> list = parseFunDeclRep();
            list.add(0,fd);
            return list;
        }
        return new LinkedList<>();
    }

    // includes are ignored, so does not need to return an AST node
    private void parseInclude() {
        System.out.println("ENTERS PARSE INCLUDE\n");
        expect(TokenClass.INCLUDE);
        expect(TokenClass.STRING_LITERAL);
        System.out.println("Consumed string literal");
    }

    private StructType parseStructDecl() {
        System.out.println("ENTERS PARSE STRUCT DECL\n");
        if(lookAhead(2).tokenClass == TokenClass.LBRA) {
            String s = parseStructType();
            expect(TokenClass.LBRA);
            List<VarDecl> varDecls = parseVarDeclRepPos();
            System.out.println("SEX!");
            expect(TokenClass.RBRA);
            expect(TokenClass.SC);
            System.out.println("KALINKALIN!!!!");
            System.out.println("Length SHOULD BE 2" + varDecls.size());
            return new StructType(s,varDecls);
        }
        else{
            String s = parseStructType();
            return new StructType(s);
        }
        //return null;
    }

    private List<VarDecl> parseVarDeclRepPos() {
        System.out.println("ENTERS PARSE VAR DECL REP POS\n");
        VarDecl vd = parseVarDecl();
        System.out.println("Should fail here!!!");
        if(accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT)){
            List<VarDecl> varDecls = parseVarDeclRepPos();
            varDecls.add(0,vd);
            System.out.println("Added!!!!!");
            return varDecls;
        }
        List<VarDecl> singList = new LinkedList<>();
        singList.add(vd);
        return singList;
    }

    private VarDecl parseVarDecl() {
        System.out.println("ENTERS PARSE VAR DECL\n");
        Type t = parseType();
        System.out.println("Parsed type");
        Token tok = expect(TokenClass.IDENTIFIER);
        if(tok != null) {
            System.out.println("SKIING");
            if (tok.tokenClass == TokenClass.IDENTIFIER && accept(TokenClass.SC)) {
                nextToken();
                String s = tok.data;
                return new VarDecl(t, s);
            } else {
                return new VarDecl(parseArrayType(t),tok.data);
            }
        }
        return null;
    }

    private ArrayType parseArrayType(Type t){
        System.out.println("Enters parse array type");
        Token tok = expect(TokenClass.LSBR);
        Token tok1 = expect(TokenClass.INT_LITERAL);
        int i = 0;
        if(tok1 != null) {
            i = Integer.parseInt(tok1.data);
        }
        expect(TokenClass.RSBR);
        expect(TokenClass.SC);
        return new ArrayType(t,i);
    }

    /*
    private void parseOption(){
        System.out.println("ENTERS PARSE OPTION\n");
        if(accept(TokenClass.SC)){
            nextToken();
        }
        else{
            expect(TokenClass.LSBR);
            expect(TokenClass.INT_LITERAL);
            expect(TokenClass.RSBR);
            expect(TokenClass.SC);
        }
    }
    */

    private FunDecl parseFunDecls() {
        System.out.println("ENTERS PARSE FUN DECL\n");
        if(accept(TokenClass.INT,TokenClass.CHAR,TokenClass.VOID,TokenClass.STRUCT) &&
                (lookAhead(3).tokenClass == TokenClass.LPAR || lookAhead(2).tokenClass == TokenClass.LPAR
                        || lookAhead(4).tokenClass == TokenClass.LPAR)) {
            Type t = parseType();
            System.out.println("FINISHED TYPE CALL\n");
            String s = expect(TokenClass.IDENTIFIER).toString();
            expect(TokenClass.LPAR);
            List<VarDecl> list = parseParams();
            expect(TokenClass.RPAR);
            Block b = parseBlock();
            return new FunDecl(t,s,list,b);
        }
        return null;
    }

    private Type parseType() {
        System.out.println("ENTERS PARSE TYPE\n");
        Type thingToReturn = null;
        if (accept(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID)){
            Token t = expect(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID);
            if(t.tokenClass == TokenClass.INT) {
                thingToReturn = BaseType.INT;
                //parseStarRep();
            }
            if(t.tokenClass == TokenClass.CHAR) {
                thingToReturn = BaseType.CHAR;
                //parseStarRep();
            }
            if(t.tokenClass == TokenClass.VOID) {
                thingToReturn = BaseType.VOID;
                //parseStarRep();
            }
        }
        else if(accept(TokenClass.STRUCT)){
            String s = parseStructType();
            thingToReturn = new StructType(s);
            //parseStarRep();
        }
        else{
            error();
        }
        if(accept(TokenClass.ASTERIX)){
            System.out.println("SHould go here");
            nextToken();
            return new PointerType(thingToReturn);
        }
        return thingToReturn;
    }


    private void parseStarRep() {
        System.out.println("ENTERS PARSE STAR REP\n");
        if(accept(TokenClass.ASTERIX)){
            nextToken();
            System.out.println("Consumed a star");
        }
    }


    private String parseStructType() {
        System.out.println("Enters parse struct type");
        expect(TokenClass.STRUCT);
        String s = expect(TokenClass.IDENTIFIER).toString();
        return s;

    }

    private List<VarDecl> parseParams(){
        System.out.println("parse PARAMS");
        List<VarDecl> list = parseParamsOpt();
        return list;
    }

    private List<VarDecl> parseParamsOpt(){
        System.out.println("PARSE PARAMS OPT");
        if (accept(TokenClass.INT, TokenClass.CHAR, TokenClass.VOID, TokenClass.STRUCT)) {
            Type t = parseType();
            String s = expect(TokenClass.IDENTIFIER).toString();
            VarDecl vd = new VarDecl(t,s);
            List<VarDecl> list = parseGroupingRep();
            list.add(0,vd);
            return list;
        }
        return new LinkedList<>();
    }

    private List<VarDecl> parseGroupingRep(){
        System.out.println("ENTERS PARSE Grouping Rep\n");
        if(accept(TokenClass.COMMA)){
            nextToken();
            Type t = parseType();
            String s = expect(TokenClass.IDENTIFIER).toString();
            VarDecl vd = new VarDecl(t,s);
            List<VarDecl> list = parseGroupingRep();
            list.add(0,vd);
            return list;
        }
        return new LinkedList<>();
    }

    private Stmt parseStmt(){
        System.out.println("Enters parse statement");
        if(accept(TokenClass.LBRA)){
            Block b = parseBlock();
            return b;
        }
        else if(accept(TokenClass.WHILE)){
            nextToken();
            expect(TokenClass.LPAR);
            Expr e = parseExp();
            expect(TokenClass.RPAR);
            Stmt s = parseStmt();
            return new While(e,s);
        }
        else if(accept(TokenClass.IF)){
            nextToken();
            expect(TokenClass.LPAR);
            Expr e = parseExp();
            expect(TokenClass.RPAR);
            Stmt s = parseStmt();
            Stmt sOpt = parseElseOptional();
            return new If(e,s,sOpt);
        }
        else if(accept(TokenClass.RETURN)){
            nextToken();
            Expr e = parseExpOptional();
            expect(TokenClass.SC);
            return new Return(e);
        }

        else {
            Expr e = parseExp();
            if(accept(TokenClass.SC)){
                nextToken();
                return new ExprStmt(e);
            }
            else if (accept(TokenClass.ASSIGN)){
                System.out.println("DANIEL");
                expect(TokenClass.ASSIGN);             // this is an assignment statement
                if(e instanceof VarExpr || e instanceof FieldAccessExpr ||
                        e instanceof ArrayAccessExpr || e instanceof ValueAtExpr) {
                    Expr e1 = parseExp();
                    System.out.println("INFORMATICS");
                    expect(TokenClass.SC);
                    System.out.println("Consumed a semicolon");
                    return new Assign(e, e1);
                }
                else{
                    System.out.println("Should go here");
                    error();
                }
            }
            else
                return parseStmt();
        }
        return null;
    }

    private Expr parseExpOptional(){
        System.out.println("ENTERS PARSE Exp Optional\n");
        if(accept(TokenClass.LPAR, TokenClass.IDENTIFIER, TokenClass.INT_LITERAL, TokenClass.MINUS, TokenClass.CHAR_LITERAL, TokenClass.STRING_LITERAL, TokenClass.SIZEOF, TokenClass.ASTERIX)){
            Expr e = parseExp();
            return e;
        }
        return null;
    }

    private Stmt parseElseOptional(){
        System.out.println("ENTERS PARSE else optional\n");
        if(accept(TokenClass.ELSE)) {
            nextToken();
            Stmt s = parseStmt();
            return s;
        }
        return null;
    }

    private Block parseBlock(){
        System.out.println("ENTERED PARSE BLOCK");
        expect(TokenClass.LBRA);
        System.out.println("Consumed a left bracket");
        List<VarDecl> listV = parseVarDeclRep();
        List<Stmt> listS = parseStmtRep();
        System.out.println("COMPUTER SCIENCE");
        expect(TokenClass.RBRA);
        return new Block(listV,listS);
    }

    private List<Stmt> parseStmtRep(){
        System.out.println("Enters parse statement rep");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.CHAR_LITERAL,
                TokenClass.STRING_LITERAL,TokenClass.SIZEOF,TokenClass.ASTERIX,TokenClass.LBRA,
                TokenClass.WHILE,TokenClass.IF,TokenClass.RETURN, TokenClass.MINUS,TokenClass.INT_LITERAL)) {
            Stmt s = parseStmt();
            List<Stmt> list = parseStmtRep();
            list.add(0,s);
            return list;
        }
        return new LinkedList<>();
    }

    private Expr parseExp(){
        System.out.println("Enters parse exp");
        Expr left = parseT();
        while(accept(TokenClass.OR)) {
            Op o = parseOp();
            Expr exprNext = parseT();
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }

        return left;
    }

    /*
    private BinOp parseN(){
        System.out.println("ENTERS PARSE N\n");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            Expr e = parseT();
            Op o = parseOp();
            Expr e1 = parseN();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp(){
        System.out.println("ENTERS PARSE Op\n");
        Token token = expect(TokenClass.OR);
        if(token.tokenClass == TokenClass.OR){
            return Op.OR;
        }
        return null;
    }

    private Expr parseT(){
        System.out.println("Enters parseT");
        Expr left = parseT1();
        while(accept(TokenClass.AND)) {
            Op o = parseOp1();
            Expr exprNext = parseT1();
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }
        return left;
    }

    /*
    private BinOp parseN1(){
        System.out.println("ENTERS PARSE N1\n");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            System.out.println("Enters THIS OPERATOR SECTION");
            Expr e = parseT1();
            Op o = parseOp1();
            Expr e1 = parseN1();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp1(){
        System.out.println("Enters parse op1\n");
        Token t = expect(TokenClass.AND);
        if(t.tokenClass == TokenClass.AND){
            return Op.AND;
        }
        return null;
    }

    private Expr parseT1(){
        System.out.println("Enters parseT1");
        Expr left = parseT2();
        while(accept(TokenClass.NE,TokenClass.EQ)) {
            Op o = parseOp2();
            Expr exprNext = parseT2();
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }
        return left;
    }

    /*
    private BinOp parseN2(){
        System.out.println("ENTERS PARSE N2\n");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            Expr e = parseT2();
            Op o = parseOp2();
            Expr e1 = parseN2();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp2(){
        System.out.println("ENTERS PARSE Op2\n");
        Token t = expect(TokenClass.NE,TokenClass.EQ);
        if(t.tokenClass == TokenClass.NE){
            return Op.NE;
        }
        if(t.tokenClass == TokenClass.EQ){
            return Op.EQ;
        }
        return null;
    }

    private Expr parseT2(){
        System.out.println("Enters parseT2");
        Expr left = parseT3();
        while(accept(TokenClass.LT,TokenClass.GT,TokenClass.LE,TokenClass.GE)) {
            Op o = parseOp3();
            Expr exprNext = parseT3();
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }
        return left;
    }

    /*
    private BinOp parseN3(){
        System.out.println("ENTERS PARSE N3\n");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            Expr e = parseT3();
            Op o = parseOp3();
            Expr e1 = parseN3();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp3(){
        System.out.println("ENTERS PARSE Op3\n");
        Token t = expect(TokenClass.LT,TokenClass.GT,TokenClass.LE,TokenClass.GE);
        if(t.tokenClass == TokenClass.LT){
            return Op.LT;
        }
        if(t.tokenClass == TokenClass.GT){
            return Op.GT;
        }
        if(t.tokenClass == TokenClass.LE){
            return Op.LE;
        }
        if(t.tokenClass == TokenClass.GE){
            return Op.GE;
        }
        return null;
    }

    private Expr parseT3(){
        System.out.println("Enters parseT3");
        Expr left = parseT4();
        while(accept(TokenClass.PLUS,TokenClass.MINUS)) {
            Op o = parseOp4();
            Expr exprNext = parseT4();
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }
        return left;
    }

    /*
    private BinOp parseN4(){
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            Expr e = parseT4();
            Op o = parseOp4();
            Expr e1 = parseN4();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp4(){
        System.out.println("ENTERS PARSE Op4\n");
        Token t  = expect(TokenClass.PLUS,TokenClass.MINUS);
        if(t.tokenClass == TokenClass.PLUS){
            return Op.ADD;
        }
        if(t.tokenClass == TokenClass.MINUS){
            return Op.SUB;
        }
        return null;
    }

    private Expr parseT4(){
        System.out.println("Enters parseT4");
        Expr left = parseT5();
        while(accept(TokenClass.LSBR,TokenClass.DOT)){
            Token t = expect(TokenClass.LSBR,TokenClass.DOT);
            if(t.tokenClass == TokenClass.LSBR){
                Expr rest = parseExp();
                expect(TokenClass.RSBR);
                left = new ArrayAccessExpr(left,rest);
            }
            if(t.tokenClass == TokenClass.DOT){
                String s = expect(TokenClass.IDENTIFIER).data;
                if(s != null) {
                    left = new FieldAccessExpr(left, s);
                }
            }

        }
        while(accept(TokenClass.ASTERIX,TokenClass.DIV,TokenClass.REM)) {
            Op o = parseOp5();
            Expr exprNext = parseT5();
            while(accept(TokenClass.LSBR,TokenClass.DOT)){
                Token t = expect(TokenClass.LSBR,TokenClass.DOT);
                if(t.tokenClass == TokenClass.LSBR){
                    Expr rest = parseExp();
                    expect(TokenClass.RSBR);
                    left = new ArrayAccessExpr(left,rest);
                }
                if(t.tokenClass == TokenClass.DOT){
                    String s = expect(TokenClass.IDENTIFIER).data;
                    if(s != null) {
                        left = new FieldAccessExpr(left, s);
                    }
                }
            }
            left = new BinOp(left, o, exprNext);
            exprConsumed = left;
        }
        return left;
    }


    /*
    private BinOp parseN5(){
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX,TokenClass.LSBR,
                TokenClass.DOT,TokenClass.SIZEOF)){
            Expr e = parseT5();
            Op o = parseOp5();
            Expr e1 = parseN5();
            exprConsumed = new BinOp(e,o,e1);
            return new BinOp(e,o,e1);
        }
        return null;
    }
    */

    private Op parseOp5(){
        System.out.println("Enters parse op5");
        Token t  = expect(TokenClass.ASTERIX,TokenClass.DIV,TokenClass.REM);
        if(t.tokenClass == TokenClass.ASTERIX){
            return Op.MUL;
        }
        if(t.tokenClass == TokenClass.DIV){
            return Op.DIV;
        }
        if(t.tokenClass == TokenClass.REM){
            return Op.MOD;
        }
        return null;
    }

    private Expr parseT5(){
        System.out.println("Enters parse T5");
        if(accept(TokenClass.LPAR)){
            if(lookAhead(1).tokenClass == TokenClass.INT || lookAhead(1).tokenClass == TokenClass.CHAR
                    || lookAhead(1).tokenClass == TokenClass.VOID || lookAhead(1).tokenClass == TokenClass.STRUCT) {
                TypecastExpr t = parseTypeCast();
                return t;
            }
            else{
                nextToken();
                Expr e = parseExp();
                expect(TokenClass.RPAR);
                return e;
            }
        }
        else if(accept(TokenClass.MINUS)){
            nextToken();
            Token t = expect(TokenClass.IDENTIFIER,TokenClass.INT_LITERAL);
            if(t.tokenClass == TokenClass.INT_LITERAL) {
                int i = Integer.parseInt(t.data);
                IntLiteral iL = new IntLiteral(i);
                return new BinOp(new IntLiteral(0),Op.SUB, iL);
            }
            if(t.tokenClass == TokenClass.IDENTIFIER){
                String s = t.data;
                return new BinOp(new IntLiteral(0),Op.SUB, new VarExpr(s));
            }
        }

        else if(accept(TokenClass.ASTERIX)){
            nextToken();
            Expr e = parseExp();
            return new ValueAtExpr(e);
        }

        else if(accept(TokenClass.SIZEOF)){
            SizeOfExpr s = parseSizeOf();
            return s;
        }

        else if(accept(TokenClass.IDENTIFIER) && lookAhead(1).tokenClass == TokenClass.LPAR){
            System.out.println("Enters this segment!!!");
            FunCallExpr f = parseFunCall();
            return f;
        }

        else if(accept(TokenClass.LSBR,TokenClass.DOT)){
            Token t = expect(TokenClass.LSBR,TokenClass.DOT);
            if(t.tokenClass == TokenClass.LSBR && exprConsumed != null){
                Expr e2 = parseExp();
                expect(TokenClass.RSBR);
                System.out.println("SOMETHING GOES WRONG HERE!");
                ArrayAccessExpr a = new ArrayAccessExpr(exprConsumed,e2);
                return a;
            }
            else if(t.tokenClass == TokenClass.DOT && exprConsumed != null){
                System.out.println("Should go here");
                Token token = expect(TokenClass.IDENTIFIER);
                String s = token.toString();
                FieldAccessExpr f = new FieldAccessExpr(exprConsumed,s);
                return f;
            }
        }

        else{
            System.out.println("KILLINGTON!");
            Token t = null;
            if(accept(TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL)) {
                t = expect(TokenClass.IDENTIFIER, TokenClass.INT_LITERAL, TokenClass.CHAR_LITERAL, TokenClass.STRING_LITERAL);
                if(t.tokenClass == TokenClass.INT_LITERAL){
                    System.out.println("OKEMO!");
                    return new IntLiteral(Integer.parseInt(t.data));
                }
                if(t.tokenClass == TokenClass.STRING_LITERAL){
                    System.out.println("This!!");
                    return new StrLiteral(t.data);
                }
                if(t.tokenClass == TokenClass.CHAR_LITERAL){
                    System.out.println("STOWE!");
                    return new ChrLiteral(t.data.charAt(0));

                }
                if(t.tokenClass == TokenClass.IDENTIFIER){
                    System.out.println("Sofia");
                    return new VarExpr(t.data);
                }
            }
            else {
                System.out.println("KALIN");
                error();
            }
        }
        return null;
    }

    private FunCallExpr parseFunCall(){
        System.out.println("ENTERS PARSE FUN CALL\n");
        String funcName = expect(TokenClass.IDENTIFIER).toString();
        expect(TokenClass.LPAR);
        List<Expr> e = parseExpOp();
        expect(TokenClass.RPAR);
        System.out.println("Consumed this");
        return new FunCallExpr(funcName,e);
    }

    private List<Expr> parseExpOp(){
        System.out.println("ENTERS PARSE Exp Op\n");
        if(accept(TokenClass.LPAR,TokenClass.IDENTIFIER,TokenClass.INT_LITERAL,TokenClass.MINUS,
                TokenClass.CHAR_LITERAL,TokenClass.STRING_LITERAL,TokenClass.ASTERIX, TokenClass.SIZEOF)) {
            Expr e = parseExp();
            System.out.println(e);
            List<Expr> list = parseCommaExpRep();
            list.add(0,e);
            return list;
        }
        return new LinkedList<>();
    }

    private List<Expr> parseCommaExpRep(){
        System.out.println("ENTERS PARSE Comma Exp Rep\n");
        if(accept(TokenClass.COMMA)){
            nextToken();
            Expr e = parseExp();
            List<Expr> list = parseCommaExpRep();
            list.add(0,e);
            return list;
        }
        return new LinkedList<>();
    }

    private SizeOfExpr parseSizeOf(){
        System.out.println("ENTERS PARSE Size Of\n");
        expect(TokenClass.SIZEOF);
        expect(TokenClass.LPAR);
        Type t = parseType();
        expect(TokenClass.RPAR);
        return new SizeOfExpr(t);
    }

    private TypecastExpr parseTypeCast(){
        System.out.println("ENTERS PARSE Type Cast\n");
        expect(TokenClass.LPAR);
        Type t = parseType();
        expect(TokenClass.RPAR);
        Expr e = parseExp();
        return new TypecastExpr(t,e);
    }
}