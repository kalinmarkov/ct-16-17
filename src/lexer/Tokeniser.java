package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author cdubach
 */
public class Tokeniser {

    private Scanner scanner;

    private int error = 0;
    public int getErrorCount() {
	return this.error;
    }
    private boolean hashtag = false;     // used for the #include token
    private boolean commentStartedButNotFinished = false;

    private HashMap<String,Token> stringToTokenMap = new HashMap<String,Token>();
    private HashMap<String,Token> charToTokenMap = new HashMap<String,Token>();


    public Tokeniser(Scanner scanner) {
        this.scanner = scanner;
        fillStringDict();
        fillCharDict();
    }

    private void error(char c, int line, int col) {
        System.out.println("Lexing error: unrecognised character ("+c+") at "+line+":"+col);
	error++;
    }


    public Token nextToken() {
        Token result;
        try {
             result = next();
        } catch (EOFException eof) {
            // end of file, nothing to worry about, just return EOF token
            return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            // something went horribly wrong, abort
            System.exit(-1);
            return null;
        }
        return result;
    }

    /*
     * To be completed
     */
    private Token next() throws IOException {

        int line = scanner.getLine();
        int column = scanner.getColumn();

        // get the next character
        char c = scanner.next();

        // skip white spaces
        if (Character.isWhitespace(c))
            return next();

        // handles comments
        if (c == '/' && scanner.peek() == '/' ){
            scanner.next();
            while(scanner.peek() != '\n'){
                scanner.next();
            }
            scanner.next();
            return next();
        }

        if(c == '/' && scanner.peek() == '*' || commentStartedButNotFinished == true){
            if(commentStartedButNotFinished == false) {
                scanner.next();
                scanner.next();
            }
            while(scanner.peek() != '*'){
                scanner.next();
            }
            scanner.next();
            if(scanner.peek() != '/'){
                commentStartedButNotFinished = true;
                scanner.next();
            }
            else {
                commentStartedButNotFinished = false;
                scanner.next();
                scanner.next();
            }
            return next();
        }

        if(isFullMatchToCharDict(c)) {
            char next = scanner.peek();
            String next_String = c + Character.toString(next);
            if (isFullMatchToStringDict(next_String)) {
                Token currToken = stringToTokenMap.get(next_String);
                TokenClass currTokenClass = currToken.getTokenClass();
                scanner.next();
                return new Token(currTokenClass, next_String, line, column);
            }
            else {
                Token currToken = charToTokenMap.get(Character.toString(c));
                TokenClass currTokenClass = currToken.getTokenClass();
                return new Token(currTokenClass,Character.toString(c),line, column);
            }

        }

        else {
            String buildUp = Character.toString(c);
            if(Character.isDigit(c)) {
                while (Character.isDigit(scanner.peek())) {
                    char next = scanner.next();
                    buildUp = buildUp + Character.toString(next);
                }
                return new Token(TokenClass.INT_LITERAL, buildUp, line, column);
            }

            // Deals with string literals
            if(c == '\"') {
                while (scanner.peek() != '\"') {
                    char next = scanner.next();
                    if(next == '\\'){
                        if (scanner.peek() == 'n') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\\' + "n";
                        } else if (scanner.peek() == 't') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\t';
                        } else if (scanner.peek() == '\\') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\\';
                        } else if (scanner.peek() == '\'') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\'';
                        }
                          else if (scanner.peek() == 'r') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\r';

                        }
                          else if (scanner.peek() == 'b') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\b';
                        }
                          else if (scanner.peek() == 'f') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '\f';
                        }
                          else if (scanner.peek() == '"') {
                            scanner.next();  // to skip closing quote

                            buildUp = buildUp + '"';
                        }
                          else {
                            error(scanner.peek(), line, column);
                            return new Token(TokenClass.INVALID, line, column);
                        }
                    }
                    if(next != '\\')
                        buildUp = buildUp + Character.toString(next);
                }
                scanner.next();  // to skip closing quote
                return new Token(TokenClass.STRING_LITERAL, buildUp.substring(1), line, column);
            }

            // Deals with char literals
            if(c == '\'') {
                while (scanner.peek() != '\'') {
                    char next = scanner.next();
                    if (next == '\\') {
                        if (scanner.peek() == 'n') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\n", line, column);
                        } else if (scanner.peek() == 't') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\t", line, column);
                        } else if (scanner.peek() == '\\') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\\", line, column);
                        } else if (scanner.peek() == '\'') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\'", line, column);
                        }
                        else if (scanner.peek() == 'r') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\r", line, column);
                        }
                        else if (scanner.peek() == 'b') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\b", line, column);
                        }
                        else if (scanner.peek() == 'f') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\f", line, column);
                        }
                        else if (scanner.peek() == '"') {
                            scanner.next();  // to skip closing quote
                            if(scanner.peek() != '\''){
                                error(scanner.peek(), line, column);
                                return new Token(TokenClass.INVALID, line, column);
                            }
                            scanner.next();
                            return new Token(TokenClass.CHAR_LITERAL, "\"", line, column);
                        }
                        else {
                            error(scanner.peek(), line, column);
                            return new Token(TokenClass.INVALID, line, column);
                        }
                    }
                    buildUp = buildUp + Character.toString(next);
                }
                scanner.next();  // to skip closing quote
                if (buildUp.length() == 2) {
                    return new Token(TokenClass.CHAR_LITERAL, buildUp.substring(1), line, column);
                } else {
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }
            }
            if(Character.isLetter(c) || c =='_') {
                while (Character.isDigit(scanner.peek()) || Character.isLetter(scanner.peek()) || scanner.peek() == '_'){
                    char next = scanner.next();
                    buildUp = buildUp + Character.toString(next);
                }
                if(!stringToTokenMap.containsKey(buildUp) && hashtag == true) {
                    hashtag = false;
                    return new Token(TokenClass.INCLUDE, "#" + buildUp, line, column);
                }
                if(!stringToTokenMap.containsKey(buildUp) && hashtag == false) {
                    return new Token(TokenClass.IDENTIFIER, buildUp, line, column);
                }
                else {
                    Token currToken = stringToTokenMap.get(buildUp);
                    TokenClass currTokenClass = currToken.getTokenClass();
                    return new Token(currTokenClass, buildUp,line, column);
                }
            }
            if(c == '!') {
                if (scanner.peek() == '=') {
                    scanner.next();
                    return new Token(TokenClass.NE, line, column);
                }
            }
            if(c == '&') {
                if (scanner.peek() == '&') {
                    scanner.next();
                    return new Token(TokenClass.AND, line, column);
                }
            }
            if(c == '|') {
                if (scanner.peek() == '|') {
                    scanner.next();
                    return new Token(TokenClass.OR, line, column);
                }
            }
            if(c == '#') {
                if(scanner.peek() == 'i') {
                    hashtag = true;
                    return next();
                }
                else {
                    error(c, line, column);
                    return new Token(TokenClass.INVALID, line, column);
                }
            }
        }

        // if we reach this point, it means we did not recognise a valid token
        error(c, line, column);
        return new Token(TokenClass.INVALID, line, column);

    }

    public boolean isFullMatchToStringDict(String s){
        return stringToTokenMap.containsKey(s);

    }

    public boolean isFullMatchToCharDict(char c){
        String cAsAString = Character.toString(c);
        return charToTokenMap.containsKey(cAsAString);

    }

    public void fillStringDict(){
        this.stringToTokenMap.put("int",new Token(TokenClass.INT,0,0));
        this.stringToTokenMap.put("void",new Token(TokenClass.VOID,0,0));
        this.stringToTokenMap.put("char",new Token(TokenClass.CHAR,0,0));
        this.stringToTokenMap.put("if",new Token(TokenClass.IF,0,0));
        this.stringToTokenMap.put("else",new Token(TokenClass.ELSE,0,0));
        this.stringToTokenMap.put("while",new Token(TokenClass.WHILE,0,0));
        this.stringToTokenMap.put("return",new Token(TokenClass.RETURN,0,0));
        this.stringToTokenMap.put("struct",new Token(TokenClass.STRUCT,0,0));
        this.stringToTokenMap.put("sizeof",new Token(TokenClass.SIZEOF,0,0));
        this.stringToTokenMap.put("#include",new Token(TokenClass.INCLUDE,0,0));
        this.stringToTokenMap.put("&&",new Token(TokenClass.AND,0,0));
        this.stringToTokenMap.put("||",new Token(TokenClass.OR,0,0));
        this.stringToTokenMap.put("==",new Token(TokenClass.EQ,0,0));
        this.stringToTokenMap.put("!=",new Token(TokenClass.NE,0,0));
        this.stringToTokenMap.put("<=",new Token(TokenClass.LE,0,0));
        this.stringToTokenMap.put(">=",new Token(TokenClass.GE,0,0));
    }

    public void fillCharDict(){
        this.charToTokenMap.put(Character.toString('='),new Token(TokenClass.ASSIGN,0,0));
        this.charToTokenMap.put(Character.toString('{'),new Token(TokenClass.LBRA,0,0));
        this.charToTokenMap.put(Character.toString('}'),new Token(TokenClass.RBRA,0,0));
        this.charToTokenMap.put(Character.toString('('),new Token(TokenClass.LPAR,0,0));
        this.charToTokenMap.put(Character.toString(')'),new Token(TokenClass.RPAR,0,0));
        this.charToTokenMap.put(Character.toString('['),new Token(TokenClass.LSBR,0,0));
        this.charToTokenMap.put(Character.toString(']'),new Token(TokenClass.RSBR,0,0));
        this.charToTokenMap.put(Character.toString(';'),new Token(TokenClass.SC,0,0));
        this.charToTokenMap.put(Character.toString(','),new Token(TokenClass.COMMA,0,0));
        this.charToTokenMap.put(Character.toString('<'),new Token(TokenClass.LT,0,0));
        this.charToTokenMap.put(Character.toString('>'),new Token(TokenClass.GT,0,0));
        this.charToTokenMap.put(Character.toString('+'),new Token(TokenClass.PLUS,0,0));
        this.charToTokenMap.put(Character.toString('-'),new Token(TokenClass.MINUS,0,0));
        this.charToTokenMap.put(Character.toString('*'),new Token(TokenClass.ASTERIX,0,0));
        this.charToTokenMap.put(Character.toString('/'),new Token(TokenClass.DIV,0,0));
        this.charToTokenMap.put(Character.toString('%'),new Token(TokenClass.REM,0,0));
        this.charToTokenMap.put(Character.toString('.'),new Token(TokenClass.DOT,0,0));
    }

}