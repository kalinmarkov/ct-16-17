package gen;

import ast.*;
import gen.ScopeGen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class CodeGenerator implements ASTVisitor<Register> {

    public ScopeGen offsets;
    public ScopeGen offsetsGlobal;
    public boolean isGlobalVariable = true;
    public int labelCounter = 0;
    public boolean cameFromWhile = false;
    public Map<String,Integer> functParams = new HashMap<String,Integer>();
    public boolean nestedBlock = false;
    public int numFunctArgs = 0;
    public int stringCount = 0;

    /*
     * Simple register allocator.
     */

    // contains all the free temporary registers
    private Stack<Register> freeRegs = new Stack<Register>();

    public CodeGenerator() {
        freeRegs.addAll(Register.tmpRegs);
        offsets = new ScopeGen();
        offsetsGlobal = new ScopeGen();
    }

    private class RegisterAllocationError extends Error {}

    private Register getRegister() {
        try {
            return freeRegs.pop();
        } catch (EmptyStackException ese) {
            throw new RegisterAllocationError(); // no more free registers, bad luck!
        }
    }

    private void freeRegister(Register reg) {
        freeRegs.push(reg);
    }


    private PrintWriter writer; // use this writer to output the assembly instructions


    public void emitProgram(Program program, File outputFile) throws FileNotFoundException {
        System.out.println("Starts the gen section and output file is " + outputFile);
        writer = new PrintWriter(outputFile);

        visitProgram(program);
        writer.close();
    }

    @Override
    public Register visitBaseType(BaseType bt) {
        return null;
    }

    @Override
    public Register visitStructType(StructType st) {
        return null;
    }

    @Override
    public Register visitBlock(Block b) {
        System.out.println("GEN BLOCK");

        ScopeGen oldOffsets = null;

        if(!nestedBlock && !cameFromWhile){
            oldOffsets = offsets;
            offsets = new ScopeGen(offsets);
        }
        if(nestedBlock || cameFromWhile){
            oldOffsets = offsets;
            offsets = new ScopeGen(offsets,offsets.getOffset());
        }

        for(Map.Entry<String, Integer> entry: functParams.entrySet()) {
            offsets.put(entry.getKey(),entry.getValue());
        }

        for (VarDecl v : b.varDecls) {
            v.accept(this);
        }

        for(Stmt s : b.stmts){
            if (s instanceof Block) {
                //functParams.clear();
                nestedBlock = true;
            }
            s.accept(this);     // visit statement s
        }

        // need to free the local variables from the stack when we are done with the block
        for (VarDecl v : b.varDecls) {
            offsets.decrementOffset(4);
            writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "4");
        }
        offsets = oldOffsets;
        return null;
    }

    @Override
    public Register visitFunDecl(FunDecl p) {
        System.out.println("GEN FUN DECL");
        isGlobalVariable = false;
        nestedBlock = false;

        writer.println(p.name + ":");

        if(!p.name.equals("main")) {
            // need to store the frame pointer and the return address to the stack so that we can return to them later
            writer.println("sw  " + "$fp" + ", " + "0($sp)");
            writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-4");
            writer.println("sw  " + "$ra" + ", " + "0($sp)");
            writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-4");
        }

        // put the parameters into the hashmap of the current function we are in, relative to where the new frame pointer will be
        int remainingParams = p.params.size();
        numFunctArgs = remainingParams;
        for(VarDecl v : p.params){
            functParams.put(v.varName, 4 * (2 + remainingParams));

            remainingParams = remainingParams - 1;
        }

        // frame pointers point to beginning of local variables of current function
        writer.println("move    " + "$fp" + ", " + "$sp");

        p.block.accept(this);


        functParams.clear();

        if(!p.name.equals("main")) {
            writer.println("lw  " + "$ra" + ", " + "4($fp)");
            writer.println("lw  " + "$fp" + ", " + "8($fp)");

            // throw away the storage of fp and ra as well
            writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "8");
        }

        writer.println("addi    " + "$sp" + ", " + "$sp" + "," + 4*numFunctArgs);


        if(!p.name.equals("main")) {
            writer.println("jr  " + "$ra");
        }

        if(p.name.equals("main")){
            writer.println("li    " + "$v0" + ", " + "10");
            writer.println("syscall");
        }
        return null;
    }

    @Override
    public Register visitProgram(Program p) {
        System.out.println("GEN PROGRAM");
        writer.println(".data");
        for (StructType st : p.structTypes) {
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }

        writer.println(".text");

        writer.println("j    " + "main");

        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Register visitVarDecl(VarDecl vd) {
        System.out.println("GEN VAR DECL");
        if(isGlobalVariable){
            if(vd.type == BaseType.INT || vd.type == BaseType.CHAR){
                writer.println(vd.varName + ":  " + ".word" + " 0");
                offsetsGlobal.put(vd.varName);
                offsetsGlobal.incrementOffset(4);
            }
            else if(vd.type == BaseType.VOID){
                writer.println(vd.varName + ":  " + ".space" + " 1");
                offsetsGlobal.put(vd.varName);
                offsetsGlobal.incrementOffset(1);
            }
            else if (vd.type instanceof ArrayType){
                int i = determineAmountOfOffset((ArrayType) vd.type);
                writer.println(vd.varName + ":  " + ".space " + i);
                offsetsGlobal.put(vd.varName);
                offsetsGlobal.incrementOffset(4);
            }
            else if (vd.type instanceof PointerType) {
                writer.println(vd.varName + ":  " + ".space " + 4);
                offsetsGlobal.put(vd.varName);
                offsetsGlobal.incrementOffset(4);
            }
            // This is a structype
            else{
                writer.println(vd.varName + ":  " + ".word" + " 0");    //template definition - to be fixed later
                offsetsGlobal.put(vd.varName);
                offsetsGlobal.incrementOffset(4);
            }


        }

        if(!isGlobalVariable) {
            offsets.put(vd.varName);
            if (vd.type == BaseType.INT || vd.type == BaseType.CHAR) {
                offsets.put(vd.varName);
                offsets.incrementOffset(4);
                writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-4");
            }
            else if (vd.type == BaseType.VOID) {
                offsets.put(vd.varName);
                offsets.incrementOffset(1);
                writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-1");
            }
            else if (vd.type instanceof ArrayType) {
                int i = determineAmountOfOffset((ArrayType) vd.type);
                offsets.put(vd.varName);
                offsets.incrementOffset(i);
                writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-" + i);
            } else if (vd.type instanceof PointerType) {
                offsets.put(vd.varName);
                offsets.incrementOffset(4);
                writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-" + 4);
            }
            // this is a structype - needs to be fixed
            else {
                offsets.put(vd.varName);
                offsets.incrementOffset(4);
                writer.println("addi    " + "$sp" + ", " + "$sp" + ", " + "-4");
            }
        }
        return null;

    }


    public int determineAmountOfOffset(ArrayType a){
        if(a.type == BaseType.INT){
            return 4 * a.numElements;
        }
        else if(a.type == BaseType.CHAR){
            return 4 * a.numElements;
        }
        // Array of structures
        else{
            return 4 * a.numElements;    // Wrong - to be fixed later
        }
    }

    @Override
    public Register visitVarExpr(VarExpr v) {
        System.out.println("GEN VarExpr");
        Register r = getRegister();
        if(offsets.lookup(v.name) != null){
            int offset = offsets.lookup(v.name);
            writer.println("lw  " + r.toString() + ", " + offset + "($fp)");

        }else{
            int offset = offsetsGlobal.lookup(v.name);
            writer.println("lw  " + r.toString() + ", " + offset + "($gp)");
        }
        return r;
    }

    @Override
    public Register visitPointerType(PointerType p) {
        return null;
    }

    @Override
    public Register visitArrayType(ArrayType a) {
        return null;
    }

    @Override
    public Register visitIntLiteral(IntLiteral i) {
        Register r = getRegister();
        writer.println("li  " + r.toString() + ", " + i.i);
        return r;
    }

    @Override
    public Register visitStrLiteral(StrLiteral i) {
        return null;
    }

    @Override
    public Register visitChrLiteral(ChrLiteral c) {
        Register r = getRegister();
        writer.println("li  " + r.toString() + ", " + "'" + c.c + "'");
        return r;
    }

    @Override
    public Register visitBinOp(BinOp b){
        System.out.println("GEN BIN OP");
        Register lhsReg = b.e1.accept(this);
        Register rhsReg = b.e2.accept(this);
        Register result = getRegister();
        switch(b.o){
            case ADD:
                writer.println("add   " + result.toString() + ", " + lhsReg.toString() + ", " + rhsReg.toString());
                break;

            case SUB:
                writer.println("sub   " + result.toString() + ", " + lhsReg.toString() + ", " + rhsReg.toString());
                break;

            case MUL:
                writer.println("mul   " + result.toString() + ", " + lhsReg.toString() + ", " + rhsReg.toString());
                break;

            case DIV:
                writer.println("div   " + lhsReg.toString() + ", " + rhsReg.toString());
                writer.println("mflo    " + result.toString());
                break;

            case MOD:
                writer.println("div   " + lhsReg.toString() + ", " + rhsReg.toString());
                writer.println("mfhi    " + result.toString());
                break;

            case GT:
                writer.println("bgt   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case LT:
                writer.println("blt   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case GE:
                writer.println("bge   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case LE:
                writer.println("ble   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case NE:
                writer.println("bne   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case EQ:
                writer.println("beq   " + lhsReg.toString() + ", " + rhsReg.toString() + " Target" + labelCounter);
                writer.println("li  " + result.toString() + ", " + "0");
                writer.println("b   " + "AfterTarget" + labelCounter);
                writer.println("Target" + labelCounter + ":    " + "li  " + result.toString() + ", " + "1");
                writer.println("AfterTarget" + labelCounter + ":    ");
                if(!cameFromWhile) {
                    labelCounter++;
                }
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;

            case OR:
                writer.println("or   " + result.toString() + ", " + lhsReg.toString() + ", " + rhsReg.toString());
                break;

            case AND:
                writer.println("and   " + result.toString() + ", " + lhsReg.toString() + ", " + rhsReg.toString());
                break;
        }
        freeRegister(lhsReg);
        freeRegister(rhsReg);
        return result;
    }

    @Override
    public Register visitFunCallExpr(FunCallExpr f){
        Register result = getRegister();
        if(f.str.equals("print_i")){
            if(f.expr.get(0) instanceof IntLiteral) {
                int intToPrint = ((IntLiteral) (f.expr.get(0))).i;
                writer.println("li  " + "$v0" + ", " + "1");
                writer.println("li  " + "$a0" + ", " + intToPrint);
                writer.println("syscall");
            }
            else{
                Register r = f.expr.get(0).accept(this);
                writer.println("li  " + "$v0" + ", " + "1");
                writer.println("move  " + "$a0" + ", " + r.toString());
                writer.println("syscall");
                freeRegister(r);
            }
            freeRegister(result);
            return result;
        }

        if(f.str.equals("print_s")){

            // Assuming nothing failed up to this stage, then the argument here must be a typecast expression
            String strToPrint  = ((StrLiteral)(((TypecastExpr)(f.expr.get(0))).e)).s;

            // declare another data section of the file
            writer.println(".data");
            writer.println("str" + stringCount + ":  " + ".asciiz " + "\"" + strToPrint + "\"");
            writer.println(".text");
            writer.println("la  " + "$a0" + ", " + "str" + stringCount);
            writer.println("li  " + "$v0" + ", " + 4);
            writer.println("syscall");

            stringCount++;
            freeRegister(result);
            return result;
        }

        if(f.str.equals("print_c")){
            if(f.expr.get(0) instanceof ChrLiteral) {
                char chrToPrint = ((ChrLiteral) (f.expr.get(0))).c;
                writer.println("li  " + "$v0" + ", " + "11");
                writer.println("li  " + "$a0" + ", " + "'" + chrToPrint + "'");
                writer.println("syscall");
            }
            else{
                Register r = f.expr.get(0).accept(this);
                writer.println("li  " + "$v0" + ", " + "11");
                writer.println("move  " + "$a0" + ", " + r.toString());
                writer.println("syscall");
            }
            freeRegister(result);
            return result;
        }

        if(f.str.equals("read_i")){
            writer.println("li  " + "$v0" + ", " + "5");
            writer.println("syscall");
            writer.println("move    " + result.toString() + ", " + "$v0");
            return result;
        }

        if(f.str.equals("read_c")){
            writer.println("li  " + "$v0" + ", " + "12");
            writer.println("syscall");
            writer.println("move    " + result.toString() + ", " + "$v0");
            return result;
        }

        if(f.str.equals("mcmalloc")){
            writer.println("li  " + "$v0" + ", " + "9");
            writer.println("syscall");
            writer.println("move    " + result.toString() + ", " + "$v0");
            return result;
        }

        // need to add the values of all the registers in use to the stack, so we can make sure they are consistent
        List<Register> regs = Register.tmpRegs;  // holds all of the registers
        Stack<Register> usedRegisters = new Stack<>();
        for(Register r : Register.tmpRegs){
            // if we enter this if, this means the register is not free - it is in use
            if(freeRegs.search(r) == -1){
                writer.println("sw  " + r.toString() + ", " + "0($sp)");
                offsets.incrementOffset(4);
                writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "-4");
                usedRegisters.push(r);
                freeRegister(r);
            }
        }

        // pass the arguments via stack
        for(Expr e : f.expr){
            Register r = e.accept(this);     // r will hold the value of the parameter
            writer.println("sw  " + r.toString() + ", " + "0($sp)"); // stores the actual value of the parameter onto the stack
            offsets.incrementOffset(4);
            writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "-4");
        }
        writer.println("jal    " + f.str);

        /*
        // remove the arguments from the stack
        for(Expr e : f.expr){
            offsets.decrementOffset(4);
            writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "4");
        }
        */

        freeRegs.clear();
        // restore state to how it was before the funcall
        for(Register r : Register.tmpRegs){
            // if we enter this if, this means the register is not in use
            if(usedRegisters.search(r) == -1){
                freeRegs.add(r);
            }
        }
        // remove the values of all the registers from the stack
        while(!usedRegisters.empty()){
            Register r = usedRegisters.pop();
            writer.println("lw  " + r.toString() + ", " + "4($sp)");
            //offsets.decrementOffset(4);
            writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "4");

        }

        writer.println("move    " + result.toString() + ", " + "$v0");

        return result;
    }

    @Override
    public Register visitOp(Op o) {
        return null;
    }

    @Override
    public Register visitArrayAccessExpr(ArrayAccessExpr aae) {
        System.out.println("VISITS ARRAY ACCESS EXPR");
        System.out.println(aae.type);
        System.out.println(((VarExpr)aae.array).name);
        Register r = getRegister();

        if(aae.type == BaseType.INT || aae.type == BaseType.CHAR){
            if(offsets.lookup(((VarExpr)aae.array).name) != null){
                int offset = offsets.lookup(((VarExpr)aae.array).name) - 4 * ((IntLiteral)aae.index).i;
                writer.println("lw  " + r.toString() + ", " + offset + "($fp)");

            }else{
                int offset = offsetsGlobal.lookup(((VarExpr)aae.array).name) - 4 * ((IntLiteral)aae.index).i;
                writer.println("lw  " + r.toString() + ", " + offset + "($gp)");
            }
            return r;
        }

        return null;
    }

    @Override
    public Register visitFieldAccessExpr(FieldAccessExpr fae) {

        return null;
    }

    @Override
    public Register visitValueAtExpr(ValueAtExpr v) {

        return null;
    }

    @Override
    public Register visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
        return null;
    }

    @Override
    public Register visitTypecastExpr(TypecastExpr t) {

        return null;
    }

    @Override
    public Register visitExprStmt(ExprStmt expr) {
        return expr.expr.accept(this);
    }

    @Override
    public Register visitWhile(While w) {
        cameFromWhile = true;
        writer.println("loop" + labelCounter + ":    ");
        Register r = w.expr.accept(this);
        writer.println("beqz    " + r.toString() + ", " + "afterloop" + labelCounter);
        int savedLabelCounter = labelCounter;
        labelCounter++;
        w.stmt.accept(this);
        writer.println("j   " + "loop" + savedLabelCounter);
        writer.println("afterloop" + savedLabelCounter + ":    ");
        freeRegister(r);

        cameFromWhile = false;
        return null;
    }

    @Override
    public Register visitIf(If i) {
        Register r = i.expr.accept(this);
        writer.println("beqz " + r.toString() + ", " + "else" + labelCounter);
        int savedLabelCounter = labelCounter;
        labelCounter++;

        i.stmt.accept(this);
        writer.println("b   " + "after" + savedLabelCounter);
        writer.println("else" + savedLabelCounter + ":    ");
        if(i.stmtOptional != null) {
            i.stmtOptional.accept(this);
        }
        writer.println("after" + savedLabelCounter + ":    ");
        freeRegister(r);
        return null;
    }

    @Override
    public Register visitAssign(Assign a) {
        System.out.println("GEN ASSIGN");
        Register r = a.expr1.accept(this);
        Register r1 = a.expr2.accept(this);

        writer.println("move    " + r.toString() + ", " +r1.toString());

        int offset = 0;

        if(a.expr1 instanceof VarExpr) {
            if (offsets.lookup(((VarExpr) (a.expr1)).name) != null) {
                offset = offsets.lookup(((VarExpr) (a.expr1)).name);
                writer.println("sw  " + r.toString() + ", " + offset + "($fp)");
            } else {
                offset = offsetsGlobal.lookup(((VarExpr) (a.expr1)).name);
                writer.println("sw  " + r.toString() + ", " + offset + "($gp)");
            }
        }
        if(a.expr1 instanceof ArrayAccessExpr){
            if (offsets.lookup(((VarExpr)((ArrayAccessExpr) (a.expr1)).array).name) != null) {
                offset = offsets.lookup(((VarExpr)((ArrayAccessExpr) (a.expr1)).array).name) - 4 * ((IntLiteral)((ArrayAccessExpr) a.expr1).index).i;
                writer.println("sw  " + r.toString() + ", " + offset + "($fp)");
            } else {
                offset = offsetsGlobal.lookup(((VarExpr)((ArrayAccessExpr) (a.expr1)).array).name) - 4 * ((IntLiteral)((ArrayAccessExpr) a.expr1).index).i;
                writer.println("sw  " + r.toString() + ", " + offset + "($gp)");
            }
        }
        freeRegister(r);
        freeRegister(r1);
        return null;
    }

    @Override
    public Register visitReturn(Return r) {
        System.out.println("GEN RETURN");
        if(r.exprOpt != null) {
            Register reg = r.exprOpt.accept(this);
            writer.println("move    " + "$v0" + ", " + reg.toString());
            freeRegister(reg);
        }

        writer.println("move    " + "$sp" + ", " + "$fp");
        writer.println("lw  " + "$ra" + ", " + "4($fp)");
        writer.println("lw  " + "$fp" + ", " + "8($fp)");

        // throw away the storage of fp and ra as well
        writer.println("addi    " + "$sp" + ", " + "$sp" + "," + "8");
        offsets.decrementOffset(4*numFunctArgs);
        writer.println("addi    " + "$sp" + ", " + "$sp" + "," + 4*numFunctArgs);

        writer.println("jr  " + "$ra");

        return null;
    }

}