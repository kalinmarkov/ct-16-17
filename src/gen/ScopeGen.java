package gen;


import java.util.HashMap;
import java.util.Map;


/**
 * Created by s1339217 on 08/11/16.
 */
public class ScopeGen {

    private ScopeGen outer;
    private int offset;

    private Map<String, Integer> offsetTable = new HashMap<String, Integer>();

    public int getOffset(){
        return offset;
    }

    public void setOffset(int i){
        offset = i;
    }

    public void incrementOffset(int i){
        offset = offset - i;
    }

    public void decrementOffset(int i){
        offset = offset + i;
    }

    public ScopeGen(ScopeGen outer) {
        this.outer = outer;
        this.offset = 0;
    }

    public ScopeGen(ScopeGen outer, int offset) {
        this.outer = outer;
        this.offset = offset;
    }

    public ScopeGen() {
        offset = 0;
    }

    public Map<String, Integer> getSymbolTable(){
            return offsetTable;
        }

    public Integer lookup(String name) {
        Integer s = lookupCurrent(name);
        if(s != null){
            return s;
        }
        if(s == null && outer != null){
            return outer.lookup(name);
        }
        return null;
    }

    public Integer lookupCurrent(String name) {
        if(offsetTable.get(name) != null){
            return offsetTable.get(name);
        }
        else
            return null;
    }

    public void remove(String name) {
        offsetTable.remove(name);
    }


    public void put(String name) {
        offsetTable.put(name,offset);
    }

    public void put(String name, int o){
        offsetTable.put(name,o);
    }

}


